import { Directive, HostListener } from '@angular/core';
import { PosService } from './services/pos.service';

@Directive({
  selector: '[appScannerBarcode]'
})
export class ScannerBarcodeDirective {

  private iKey = 13;
  private iString = '';
  constructor(private _PosService: PosService) { }
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {

    /* setInterval(() => {
      if (event.keyCode !== this.key) {
        this.iString = '';
      } TODO: analizar cada cuanto se limpiaria
    }, 3000); */
    if (event.keyCode === this.iKey) {
        console.log('buscar', this.iString);
        this._PosService.barCodeProduct(this.iString);
        this.iString = '';
    } else {
      this.iString += event.key;
      console.log(this.iString);
    }
  }


}
