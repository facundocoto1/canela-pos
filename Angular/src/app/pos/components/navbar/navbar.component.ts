import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../core/model/User';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() iCurrentUser: User = new User();
  constructor() { }

  ngOnInit() { }

  logout() {
    window.location.href = window.location.origin + '/' + window.location.pathname.split('/')[1] + '/auth/logout';
  }

}
