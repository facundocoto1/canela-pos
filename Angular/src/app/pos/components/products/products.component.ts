import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from '../../core/model/Product';
import { PosService } from '../../services/pos.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {

  private iProducts: Product[];

  constructor(private _PosService: PosService) { }
  dtOptions: DataTables.Settings = {};
  dtLanguage: DataTables.LanguageSettings;
  dtTrigger = new Subject<any>();


  ngOnInit() {
    // Obtengo el listado de productos
    this._PosService.initializeProducts();
    this._PosService.getProducts().subscribe(res => {
      this.iProducts = res;
      // this.dtTrigger.next();
    });
    // Subscription a la devolucion de productos al listado
    this._PosService.getProductToReturn().subscribe( res => {
      this.iProducts.push(res);
    });

    // Subscription al evento de eliminar producto del listado
    this._PosService.getProductToDelete().subscribe(res => {
      this.addProdudctToCart(res);
    });

    // Cargo la configuracion de la tabla
    // this.configureDataTable();
  }

  // Metodo para asignarle la configuracion a la tabla de producots
  // private configureDataTable() {
  //   // Defino el idioma espaniol
  //   const spanish = {
  //     'emptyTable': 'No hay productos para mostrar',
  //     'infoEmpty': 'No hay productos para mostrar',
  //     'search': 'Buscar:',
  //     'zeroRecords': 'No hay elementos que coincidan con la búsqueda',
  //     'paginate': {
  //       'first': '&laquo;&laquo;',
  //       'previous': '&laquo;',
  //       'next': '&raquo;',
  //       'last': '&raquo;&raquo;'
  //     }
  //   };

  //   this.dtOptions.language = spanish;
  //   // No muestro opciones ni informacion de cantidad
  //   this.dtOptions.info = false;
  //   // Establezco cantidad de productos a mostrar por pagina
  //   // this.dtOptions.pageLength = 8;
  //   this.dtOptions.paging = false;
  //   this.dtOptions.lengthChange = false;
  //   this.dtOptions.order = [1, 'asc'];
  // }

  // metodo para agregar un producto al carrito
  addProdudctToCart(pBarcode: string) {
    const mIndex = this.iProducts.findIndex(x => x.code_bar === pBarcode);
    if (mIndex > -1) {
      this.dtTrigger.unsubscribe();
      this._PosService.setProductToCart(this.iProducts[mIndex]);
      this.iProducts.splice(mIndex, 1);
    }
  }

}
