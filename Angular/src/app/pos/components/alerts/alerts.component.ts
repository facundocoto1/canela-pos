import { Component, OnInit } from '@angular/core';
import { Alert } from '../../core/model/Alert';
import { PosService } from '../../services/pos.service';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  constructor(private _posService: PosService) { }

  private iMessage: Alert;
  private iShowMessage = false;
  ngOnInit() {

    this._posService.getAlertToshow().subscribe( res => {
      this.iShowMessage = true;
      this.iMessage = res;
      setTimeout(() => {
        this.iShowMessage = false;
       }, 4000);
     });
   }

}
