import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../core/model/Product';
import { PosService } from '../../services/pos.service';
import { PaymentMethod } from '../../core/model/PaymentMethod';
import { TaxCondition } from '../../core/model/TaxCondition';
import { Sale } from '../../core/model/Sale';
import { User } from '../../core/model/User';
import { ProductSale } from '../../core/model/ProductSale';
import { QzTrayService } from '../../services/qz-tray-service.service';
import { Alert } from '../../core/model/Alert';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  private iProducts: Product[] = []; // Mantiene el listado de los productos a vender
  private iPaymentMethods: PaymentMethod[] = []; // Controla los metodos a pagar
  private iTaxConditions: TaxCondition[] = []; // Controla los impuestos a incluir

  private iPaymentSelected: PaymentMethod;
  private iTaxConditionSelected: TaxCondition;
  private iTotalPrice = 0; // Controla el valor total de la compra
  private iDiscount: number; // Controla el descuento a realizar

  @Input() iCurrentUser: User;
  constructor(private _PosService: PosService,
              private _QZService: QzTrayService) { }

  ngOnInit() {
    this._PosService.getCart().subscribe ( res => {
      this.iProducts.push(res);
      this.calculatePrice();

    });

    this._PosService.getTaxConditions().subscribe(res => {
      this.iTaxConditions = res['result'];
    });

    this._PosService.getPaymentsMethods().subscribe(res => {
      this.iPaymentMethods = res['result'];
    });
   }


  calculatePrice() {
    this.iDiscount > 100 ? this.iDiscount = 100 :
    this.iDiscount < 0 ? this.iDiscount = 0 :
    this.iTotalPrice = 0;
    this.iProducts.forEach( x =>
      this.iTotalPrice += Number(x.price));
    // Intenta aplicarle descuento
      if (this.iDiscount) {
      this.iTotalPrice = this.iTotalPrice - (this.iTotalPrice * (this.iDiscount / 100));
    }
  }

  private deleteFromCart(pProductID: number) {
    const mIndex = this.iProducts.findIndex ( x => x.id === pProductID);
    if (mIndex > -1) {
      this._PosService.setProductToReturn(this.iProducts[mIndex]);
      this.iProducts.splice(mIndex, 1);
      this.calculatePrice();
    }
  }

  setPaymentMethod(pId: number) {
    const mIndex = this.iPaymentMethods.findIndex ( item => item.id === pId);
    this.iPaymentSelected = this.iPaymentMethods[mIndex];
  }

  setTaxCondition(pId: number) {

    const mIndex = this.iTaxConditions.findIndex ( item => item.id === pId);
    this.iTaxConditionSelected = this.iTaxConditions[mIndex];
  }

  confirmShop() {
    // this._QZService.printTicket();
    if (!this.iDiscount) { this.iDiscount = 0; }
    const mProducSales: ProductSale[] = [];
    this.iProducts.forEach( item => {
      const mProductSale: ProductSale = new ProductSale(1, Number(item.id));
      mProducSales.push(mProductSale);
    });
    console.log(this.iCurrentUser);
    console.log(this.iCurrentUser.id);
    const mSale: Sale = new Sale(this.iPaymentSelected.id,
                                Number(this.iCurrentUser.id),
                                this.iDiscount,
                                this.iTaxConditionSelected.id,
                                mProducSales);
    console.log(mProducSales);
    this._PosService.addNewSale(mSale).subscribe(res => {
      this._PosService.setAlertToShow(new Alert('Operación exitosa', 1));
    },
    error => this._PosService.setAlertToShow(new Alert('Hubo un error', 2)));
    this.iProducts = [];
  }

}
