import { Injectable } from '@angular/core';

// import 'rxjs/add/observable/fromPromise';
// import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs';
// import { from } from 'rxjs';



declare var qz: any;


@Injectable()
export class QzTrayService {
  constructor() {}

  private ticket =
      {
          nombreComercio: 'CANELA ROPA',
          nombreDuenio: 'SIGOT MARIELA VIVIANA',
          cuit: 'Nro.: 27-250256179-7',
          direccion: 'ROCAMORA 651 * C. DEL URUGUAY * 3260 * E.R.',
          pv: '',  // NO SE QUE ES ESTO
          nroTicket: '',
          fechayhora: '',
          productos: [],
          total: 0,
          saludo: 'GRACIAS POR SU COMPRA',
      };


  configQZ() {
      qz.security.setCertificatePromise(function (resolve, reject) {
          // Preferred method - from server
           $.ajax('./../../../assets/qz/override.crt\')?>').then(resolve, reject);
      });


      qz.security.setSignaturePromise(function (toSign) {
          return function (resolve, reject) {
              $.post('./../../../assets/qz/assets/signing/sign-message.js', { request: toSign }).then(resolve, reject);
          };
      });
  }

  /**
   * Imprime el ticket
   */
  generateTicket() {
      const cadena_final = [];
      // init ticket
      cadena_final.push('\n');
      // centrado
      cadena_final.push('\x1D' + '\x21' + '\x00');
      cadena_final.push('\x1B' + '\x61' + '\x31' + this.ticket.nombreComercio);
      cadena_final.push('\n');

      cadena_final.push('-------------------');
      cadena_final.push('\n');
      cadena_final.push(this.ticket.nombreDuenio);
      cadena_final.push('\n');
      cadena_final.push('==============================================');
      cadena_final.push('\n');
      // alinear izq
      cadena_final.push('\x1B' + '\x61' + '\x30');
      cadena_final.push('Ticket Nro. : ' + this.ticket.nroTicket);
      cadena_final.push('\n');
      cadena_final.push('Fecha : ' + this.ticket.fechayhora);
      cadena_final.push('\n');
      // centrado
      cadena_final.push('\x1B' + '\x61' + '\x31');
      cadena_final.push('----------------------------------------------');
      cadena_final.push('\n');
      cadena_final.push('//////////////  //////////////');
      cadena_final.push('\n');
      cadena_final.push('----------------------------------------------');
      cadena_final.push('\n');

      console.log(cadena_final);
      return cadena_final;
  }

  printTicket() {
      this.configQZ();
      const cadena = this.generateTicket();
      console.log(qz.websocket.isActive());
      if (qz.websocket.isActive()) {
          const config = qz.configs.create(print);
          return qz.print(config, cadena);
      } else {
          qz.websocket.connect().then(function () {
              return qz.printers.find('epson');
          })
          .then(function (printer) {
              const config = qz.configs.create(printer);
              return qz.print(config, cadena);
          })
          .catch(function (e) { console.error(e); });
      }
  }
}
