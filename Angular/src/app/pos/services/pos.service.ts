import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { User } from '../core/model/User';
import { Observable, Subject } from 'rxjs';
import { Product } from '../core/model/Product';
import { Sale } from '../core/model/Sale';
import { ProductSale } from '../core/model/ProductSale';
import { TaxCondition } from '../core/model/TaxCondition';
import { Alert } from '../core/model/Alert';

@Injectable({
  providedIn: 'root'
})
export class PosService {

  private iLoading: Subject<boolean> = new Subject<boolean>(); // Controla la visualización o no de la vista
  private iAlerts: Subject<Alert> = new Subject<Alert>(); // Controla la visualización o no de la vista
  private iCart: Subject<Product> = new Subject<Product>(); // Carrito de compras
  private iProductToReturn: Subject<Product> = new Subject<Product>(); // Productos a volver a poner en el listado
  private iProductToDelete: Subject<string> = new Subject<string>(); // Productos a quitar del listado (por codigo de barra)
  private iProducts: Subject<Product[]> = new Subject<Product[]>(); // Productos a volver a poner en el listado
  private iApiLocation =  window.location.origin + '/' + window.location.pathname.split('/')[1];
  // 'http://qa.escalantef.com/pos';

  constructor(private _httpClient: HttpClient) {
    console.log(this.iApiLocation);
   }

  // Devuelve un observable con la informacion del usuario loggeado
  getUserLogged(): Observable<User> {
    return this._httpClient.get<User>(this.iApiLocation + '/Users/getUserLogged');
  }

  // Devuelve un listado de los productos para vender
  getProductList(): Observable<Product[]> {
    return this._httpClient.get<Product[]>(this.iApiLocation + '/Products/getProducts');
  }

  getPaymentsMethods(): Observable<PaymentMethodData> {
    return this._httpClient.get<PaymentMethodData>(this.iApiLocation + '/Sales/getPaymentMethods');
  }

  getTaxConditions(): Observable<TaxCondition> {
    return this._httpClient.get<TaxCondition>(this.iApiLocation + '/Sales/getTaxConditions');
  }

  // Mantiene y setea el estado del punto de venta (cargando configuracion o no)
  getLoadingConfiguration() { return this.iLoading.asObservable(); }
  setLoadingConfiguration(pLoad: boolean) { this.iLoading.next(pLoad); }

  // Mantiene y setea el estado del punto de venta (cargando configuracion o no)
  getCart() { return this.iCart.asObservable(); }
  setProductToCart(pProduct: Product) { this.iCart.next(pProduct); }

  // Metodo para agregar un producto por scanner
  barCodeProduct(pBarcode: string) {
    this.addProductByCodeBar(pBarcode);
  }

  // Mantiene los productos a quitar del carrito y devolver al listado
  getProductToReturn() { return this.iProductToReturn.asObservable(); }
  setProductToReturn(pProducts: Product) { this.iProductToReturn.next(pProducts); }

  // Mantiene el listado de productos
  getProducts() { return this.iProducts.asObservable(); }
  initializeProducts() {
    this.getProductList().subscribe( res => {
      this.iProducts.next(res['result']);
    });
  }

    // Mantiene los productos a quitar del carrito y devolver al listado
  getProductToDelete() { return this.iProductToDelete.asObservable(); }
  addProductByCodeBar(pProductId: string) { this.iProductToDelete.next(pProductId); }

  addNewSale(pSale: Sale) {
    console.log(pSale);
    const mHeader = new HttpHeaders().set('Content-Type', 'application/json');
    const mBody = JSON.stringify(pSale);
    console.log(mBody);
    return this._httpClient.post( this.iApiLocation + '/Sales/saveSale', mBody, {headers: mHeader});

  }

  // Mantiene y setea los mensajes de error y exito
    // Mantiene los productos a quitar del carrito y devolver al listado
    getAlertToshow() { return this.iAlerts.asObservable(); }
    setAlertToShow(pAlert: Alert) { this.iAlerts.next(pAlert); }

}
