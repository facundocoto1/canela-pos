import { TestBed } from '@angular/core/testing';

import { QzTrayServiceService } from './qz-tray-service.service';

describe('QzTrayServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QzTrayServiceService = TestBed.get(QzTrayServiceService);
    expect(service).toBeTruthy();
  });
});
