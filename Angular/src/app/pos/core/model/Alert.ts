import { AlertType } from './AlertType';

export class Alert {
    message: string;
    typeMessage: AlertType;

    /**
     *
     */
    constructor(pMessage: string, pTypeMessage: AlertType) {
        this.message = pMessage;
        this.typeMessage = pTypeMessage;
    }
}
