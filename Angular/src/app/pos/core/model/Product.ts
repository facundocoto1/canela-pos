export class Product {
    id: number;
    name: string;
    description: string;
    code_bar: string;
    price: number;

    constructor(pID: number, pName: string, pDescription: string, pCodeBar: string, pPrice: number) {
        this.id = pID;
        this.name = pName;
        this.description = pDescription;
        this.code_bar = pCodeBar;
        this.price = pPrice;
    }
}
