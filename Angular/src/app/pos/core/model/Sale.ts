import { ProductSale } from './ProductSale';

export class Sale {
    id: number;
    payment_method_id: number;
    seller_id: number;
    tax_condition_id: number;
    discount: number;
    products: ProductSale[];

    /**
     *
     */
    constructor(pPaymentMethodID: number, pSellerID: number, pDiscount: number, pTaxConditionID: number, pProductSale: ProductSale[]) {
        this.payment_method_id = pPaymentMethodID;
        this.seller_id = pSellerID;
        this.discount = pDiscount;
        this.tax_condition_id = pTaxConditionID;
        this.products = pProductSale;
    }
}
