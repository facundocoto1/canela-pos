export class ProductSale {
    id: number;
    quantity: number;
    product_id: number;

    /**
     *
     */
    constructor(mQuantity: number, mProductID: number) {
        this.quantity = mQuantity;
        this.product_id = mProductID;
    }
}
