import { Component, OnInit } from '@angular/core';
import { PosService } from '../services/pos.service';
import { User } from '../core/model/User';
import { Product } from '../core/model/Product';

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.css']
})
export class PosComponent implements OnInit {

  private iLoadingConfiguration = true;
  private iCurrentUser: User;

  constructor(private _PosService: PosService) { }

  ngOnInit() {
    // Me subscribo a la carga de configuracion
    this._PosService.setLoadingConfiguration(true);
    this._PosService.getLoadingConfiguration().subscribe ( res => this.iLoadingConfiguration = res);
    // Obtengo el usuario loggeado y datos iniciales
    this._PosService.getUserLogged().subscribe(resuserLogged => {
      this.iCurrentUser = resuserLogged['result'];
      // Aviso que ya termino de cargar la data
      this._PosService.setLoadingConfiguration(false);
    },
    error => console.log(error));

   }

}
