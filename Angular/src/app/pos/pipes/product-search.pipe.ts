import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../core/model/Product';

@Pipe({
  name: 'productSearch'
})
export class ProductSearchPipe implements PipeTransform {

  transform(items: Product[], searchText: string): any[] {
    if (!items) { return []; }
    if (!searchText) { return items; }
    searchText = searchText.toLowerCase();
    return items.filter( it => {
      return (
        it.name.toLowerCase().includes(searchText) ||
        it.description.toLowerCase().includes(searchText) ||
        it.code_bar.includes(searchText)
      );
    });
   }

}
