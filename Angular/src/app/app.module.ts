import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { PosComponent } from './pos/pos/pos.component';
import { NavbarComponent } from './pos/components/navbar/navbar.component';
import { ProductsComponent } from './pos/components/products/products.component';
import { ShoppingCartComponent } from './pos/components/shopping-cart/shopping-cart.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PosService } from './pos/services/pos.service';
import { ScannerBarcodeDirective } from './pos/scanner-barcode.directive';
import { ProductSearchPipe } from './pos/pipes/product-search.pipe';
import { CommonModule } from '@angular/common';
import { QzTrayService } from './pos/services/qz-tray-service.service';
import { AlertsComponent } from './pos/components/alerts/alerts.component';






 // Rutas
 const routes: Routes = [
  { path: '**',
    redirectTo: 'ventas',
  },
  { path: 'ventas', component: PosComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    PosComponent,
    NavbarComponent,
    ProductsComponent,
    ShoppingCartComponent,
    ScannerBarcodeDirective,
    ProductSearchPipe,
    AlertsComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes, {useHash: true}),
    // RouterModule.forRoot(routes),
    DataTablesModule
  ],
  providers: [PosService, QzTrayService],
  bootstrap: [AppComponent]
})
export class AppModule { }
