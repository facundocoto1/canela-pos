(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'POS';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _pos_pos_pos_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pos/pos/pos.component */ "./src/app/pos/pos/pos.component.ts");
/* harmony import */ var _pos_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pos/components/navbar/navbar.component */ "./src/app/pos/components/navbar/navbar.component.ts");
/* harmony import */ var _pos_components_products_products_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pos/components/products/products.component */ "./src/app/pos/components/products/products.component.ts");
/* harmony import */ var _pos_components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pos/components/shopping-cart/shopping-cart.component */ "./src/app/pos/components/shopping-cart/shopping-cart.component.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _pos_services_pos_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pos/services/pos.service */ "./src/app/pos/services/pos.service.ts");
/* harmony import */ var _pos_scanner_barcode_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pos/scanner-barcode.directive */ "./src/app/pos/scanner-barcode.directive.ts");
/* harmony import */ var _pos_pipes_product_search_pipe__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pos/pipes/product-search.pipe */ "./src/app/pos/pipes/product-search.pipe.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _pos_services_qz_tray_service_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pos/services/qz-tray-service.service */ "./src/app/pos/services/qz-tray-service.service.ts");
/* harmony import */ var _pos_components_alerts_alerts_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pos/components/alerts/alerts.component */ "./src/app/pos/components/alerts/alerts.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















// Rutas
var routes = [
    { path: '**',
        redirectTo: 'ventas',
    },
    { path: 'ventas', component: _pos_pos_pos_component__WEBPACK_IMPORTED_MODULE_4__["PosComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _pos_pos_pos_component__WEBPACK_IMPORTED_MODULE_4__["PosComponent"],
                _pos_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"],
                _pos_components_products_products_component__WEBPACK_IMPORTED_MODULE_6__["ProductsComponent"],
                _pos_components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_7__["ShoppingCartComponent"],
                _pos_scanner_barcode_directive__WEBPACK_IMPORTED_MODULE_12__["ScannerBarcodeDirective"],
                _pos_pipes_product_search_pipe__WEBPACK_IMPORTED_MODULE_13__["ProductSearchPipe"],
                _pos_components_alerts_alerts_component__WEBPACK_IMPORTED_MODULE_16__["AlertsComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_14__["CommonModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true }),
                // RouterModule.forRoot(routes),
                angular_datatables__WEBPACK_IMPORTED_MODULE_8__["DataTablesModule"]
            ],
            providers: [_pos_services_pos_service__WEBPACK_IMPORTED_MODULE_11__["PosService"], _pos_services_qz_tray_service_service__WEBPACK_IMPORTED_MODULE_15__["QzTrayService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/pos/components/alerts/alerts.component.css":
/*!************************************************************!*\
  !*** ./src/app/pos/components/alerts/alerts.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pos/components/alerts/alerts.component.html":
/*!*************************************************************!*\
  !*** ./src/app/pos/components/alerts/alerts.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"iShowMessage\"\r\n      [ngClass] = \"{'alert-primary': iMessage.typeMessage === 1,\r\n                    'alert-danger': iMessage.typeMessage === 2 }\"\r\n      class=\"alert alert-dismissible alert-fixed\" role=\"alert\">\r\n  {{iMessage.message}}\r\n</div>"

/***/ }),

/***/ "./src/app/pos/components/alerts/alerts.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/pos/components/alerts/alerts.component.ts ***!
  \***********************************************************/
/*! exports provided: AlertsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertsComponent", function() { return AlertsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/pos.service */ "./src/app/pos/services/pos.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertsComponent = /** @class */ (function () {
    function AlertsComponent(_posService) {
        this._posService = _posService;
        this.iShowMessage = false;
    }
    AlertsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._posService.getAlertToshow().subscribe(function (res) {
            _this.iShowMessage = true;
            _this.iMessage = res;
            setTimeout(function () {
                _this.iShowMessage = false;
            }, 4000);
        });
    };
    AlertsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-alerts',
            template: __webpack_require__(/*! ./alerts.component.html */ "./src/app/pos/components/alerts/alerts.component.html"),
            styles: [__webpack_require__(/*! ./alerts.component.css */ "./src/app/pos/components/alerts/alerts.component.css")]
        }),
        __metadata("design:paramtypes", [_services_pos_service__WEBPACK_IMPORTED_MODULE_1__["PosService"]])
    ], AlertsComponent);
    return AlertsComponent;
}());



/***/ }),

/***/ "./src/app/pos/components/navbar/navbar.component.css":
/*!************************************************************!*\
  !*** ./src/app/pos/components/navbar/navbar.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pos/components/navbar/navbar.component.html":
/*!*************************************************************!*\
  !*** ./src/app/pos/components/navbar/navbar.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg h-100 navbar-top navbar-dark bg-dark\">\r\n  <div class=\"w-100 text-center\">\r\n      <a class=\"navbar-brand float-left\" href=\"#\">Canela - Punto de Venta</a>\r\n      <a class=\"navbar-brand text-light\"> En atención: {{iCurrentUser.username}} </a>\r\n      <a class=\"navbar-brand float-right\" href=\"#\" (click)=\"logout()\"> Salir <i class=\"fa fa-power-off\"></i> </a>\r\n  </div>  \r\n</nav>"

/***/ }),

/***/ "./src/app/pos/components/navbar/navbar.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/pos/components/navbar/navbar.component.ts ***!
  \***********************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_model_User__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/model/User */ "./src/app/pos/core/model/User.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
        this.iCurrentUser = new _core_model_User__WEBPACK_IMPORTED_MODULE_1__["User"]();
    }
    NavbarComponent.prototype.ngOnInit = function () { };
    NavbarComponent.prototype.logout = function () {
        window.location.href = window.location.origin + '/' + window.location.pathname.split('/')[1] + '/auth/logout';
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _core_model_User__WEBPACK_IMPORTED_MODULE_1__["User"])
    ], NavbarComponent.prototype, "iCurrentUser", void 0);
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/pos/components/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/pos/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/pos/components/products/products.component.css":
/*!****************************************************************!*\
  !*** ./src/app/pos/components/products/products.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pos/components/products/products.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/pos/components/products/products.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container card h-100 w-100 products\">\r\n  <div class=\"card-header alert-success text-dark w-100\">\r\n    <div class=\"row\">\r\n    <div class=\"col-6\" style=\"margin-top:  8px;\">\r\n        <i class=\"fa fa-list \" aria-hidden=\"true\"></i> \r\n        <span>Productos</span>\r\n    </div>\r\n    <div class=\"col-6\">\r\n      <input type=\"text\" [(ngModel)]=\"queryString\" class=\"float-right form-control\" placeholder=\"Buscar..\">\r\n    </div>\r\n  </div>\r\n  </div>\r\n\r\n    <div class=\"w-100 h-100\">\r\n      <!-- datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" -->\r\n      <table  class=\"table table-sm table-hover text-center w-100\">\r\n        <thead class=\"table-secondary table-sm text-dark text-center\">\r\n          <tr>\r\n            <th class=\"text-center\">Código</th>\r\n            <th class=\"text-center\">Nombre / Marca</th>\r\n            <th class=\"text-center\">Descripción</th>\r\n            <th class=\"text-center\">Precio</th>\r\n            <th class=\"text-center\">Acción</th>\r\n          </tr>\r\n        </thead>\r\n      \r\n        <tbody>\r\n          <tr *ngFor = \"let product of iProducts  | productSearch : queryString\">\r\n            <td class=\"align-middle\">{{product.code_bar}}</td>\r\n            <td class=\"align-middle\">{{product.name}}</td>\r\n            <td class=\"align-middle\">{{product.description}}</td>\r\n            <td class=\"align-middle\">${{product.price}}</td>\r\n            <td class=\"align-middle\">\r\n              <button (click) = \"addProdudctToCart(product.code_bar)\"\r\n                        class=\"btn btn-outline-success\"> \r\n                  Agregar \r\n                </button>\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pos/components/products/products.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pos/components/products/products.component.ts ***!
  \***************************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_pos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/pos.service */ "./src/app/pos/services/pos.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductsComponent = /** @class */ (function () {
    function ProductsComponent(_PosService) {
        this._PosService = _PosService;
        this.dtOptions = {};
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    ProductsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Obtengo el listado de productos
        this._PosService.initializeProducts();
        this._PosService.getProducts().subscribe(function (res) {
            _this.iProducts = res;
            // this.dtTrigger.next();
        });
        // Subscription a la devolucion de productos al listado
        this._PosService.getProductToReturn().subscribe(function (res) {
            _this.iProducts.push(res);
        });
        // Subscription al evento de eliminar producto del listado
        this._PosService.getProductToDelete().subscribe(function (res) {
            _this.addProdudctToCart(res);
        });
        // Cargo la configuracion de la tabla
        // this.configureDataTable();
    };
    // Metodo para asignarle la configuracion a la tabla de producots
    // private configureDataTable() {
    //   // Defino el idioma espaniol
    //   const spanish = {
    //     'emptyTable': 'No hay productos para mostrar',
    //     'infoEmpty': 'No hay productos para mostrar',
    //     'search': 'Buscar:',
    //     'zeroRecords': 'No hay elementos que coincidan con la búsqueda',
    //     'paginate': {
    //       'first': '&laquo;&laquo;',
    //       'previous': '&laquo;',
    //       'next': '&raquo;',
    //       'last': '&raquo;&raquo;'
    //     }
    //   };
    //   this.dtOptions.language = spanish;
    //   // No muestro opciones ni informacion de cantidad
    //   this.dtOptions.info = false;
    //   // Establezco cantidad de productos a mostrar por pagina
    //   // this.dtOptions.pageLength = 8;
    //   this.dtOptions.paging = false;
    //   this.dtOptions.lengthChange = false;
    //   this.dtOptions.order = [1, 'asc'];
    // }
    // metodo para agregar un producto al carrito
    ProductsComponent.prototype.addProdudctToCart = function (pBarcode) {
        var mIndex = this.iProducts.findIndex(function (x) { return x.code_bar === pBarcode; });
        if (mIndex > -1) {
            this.dtTrigger.unsubscribe();
            this._PosService.setProductToCart(this.iProducts[mIndex]);
            this.iProducts.splice(mIndex, 1);
        }
    };
    ProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-products',
            template: __webpack_require__(/*! ./products.component.html */ "./src/app/pos/components/products/products.component.html"),
            styles: [__webpack_require__(/*! ./products.component.css */ "./src/app/pos/components/products/products.component.css")]
        }),
        __metadata("design:paramtypes", [_services_pos_service__WEBPACK_IMPORTED_MODULE_2__["PosService"]])
    ], ProductsComponent);
    return ProductsComponent;
}());



/***/ }),

/***/ "./src/app/pos/components/shopping-cart/shopping-cart.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/pos/components/shopping-cart/shopping-cart.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pos/components/shopping-cart/shopping-cart.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/pos/components/shopping-cart/shopping-cart.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container h-100\">\r\n    <div class=\"card h-100\">\r\n             <div class=\"card-header alert-success text-dark w-100 text-center\">\r\n                 <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i>\r\n                 Compra actual\r\n                 <div class=\"clearfix\"></div>\r\n             </div>\r\n             <div class=\"container h-50\" style=\"overflow-y: auto\">\r\n                     <!-- PRODUCT -->\r\n                     <div class=\"row\">\r\n                        <table class=\"table table-sm table-hover text-center\">\r\n                            <thead class=\"table-secondary\">\r\n                              <tr>\r\n                                <th class=\"text-center\">Producto</th>\r\n                                <th class=\"text-center\">Precio</th>\r\n                                <th class=\"text-center\">Accion</th>\r\n                              </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                              <tr *ngFor = \"let product of iProducts\">\r\n                                <td class=\"align-middle\">{{product.name}}</td>\r\n                                <td class=\"align-middle\">${{product.price}}</td>\r\n                                <td> \r\n                                    <button (click) = \"deleteFromCart(product.id)\"\r\n                                            type=\"button\" \r\n                                            class=\"btn btn-outline-danger btn-xs\">\r\n                                      <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\r\n                                    </button>\r\n                                </td>\r\n                              </tr>\r\n                            </tbody>\r\n                          </table>\r\n                     </div>\r\n                     <hr>\r\n                     <div class=\"col-12\">\r\n                         <span class=\"float-right\">Precio total: <b>${{iTotalPrice}}</b></span>\r\n                    </div>\r\n                     <!-- END PRODUCT -->\r\n             </div>\r\n             <div class=\"card-footer bottom-div\">\r\n                 <div class=\"row m-3\">\r\n                     \r\n                      <div class=\"col-12\">\r\n                            <div class=\"input-group mb-3\">\r\n                                <input  type=\"text\" class=\"form-control\" \r\n                                        [(ngModel)]=\"iDiscount\"\r\n                                        name=\"iDiscount\"\r\n                                        type=\"number\"\r\n                                        min=\"0\" max=\"100\"\r\n                                        (ngModelChange)=\"calculatePrice($event)\"\r\n                                        placeholder=\"Descuento (%)\">\r\n                            </div>\r\n                        <div class=\"dropdown w-100 mb-2\">\r\n                            <button     class=\"btn btn-outline-primary dropdown-toggle w-100\"\r\n                                        type=\"button\"\r\n                                        id=\"dropdownMenuButton\" \r\n                                        data-toggle=\"dropdown\" \r\n                                        aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                                        <span *ngIf=\"!iPaymentSelected\">Forma de pago</span>\r\n                                        <span *ngIf=\"iPaymentSelected\">{{iPaymentSelected.name}}</span>                                    \r\n                            </button>\r\n\r\n                            <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\r\n                                <a *ngFor=\"let method of iPaymentMethods\"\r\n                                        class=\"dropdown-item\"\r\n                                        href=\"#\"\r\n                                        (click)=\"setPaymentMethod(method.id)\"\r\n                                        >{{method.name}}\r\n                                </a>\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"dropdown w-100 mb-2\">\r\n                            <button     class=\"btn btn-outline-primary dropdown-toggle w-100\"\r\n                                        type=\"button\"\r\n                                        id=\"dropdownMenuButton\" \r\n                                        data-toggle=\"dropdown\" \r\n                                        aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                                        <span *ngIf=\"!iTaxConditionSelected\">Condición Impositiva</span>\r\n                                        <span *ngIf=\"iTaxConditionSelected\">{{iTaxConditionSelected.name}}</span>                                    \r\n                            </button>\r\n\r\n                            <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\r\n                                <a *ngFor=\"let tax of iTaxConditions\"\r\n                                        class=\"dropdown-item\"\r\n                                        href=\"#\"\r\n                                        (click)=\"setTaxCondition(tax.id)\"\r\n                                        >{{tax.name}}\r\n                                </a>\r\n\r\n                            </div>\r\n                        </div>\r\n                        <!-- <button class=\"btn btn-success w-100\" \r\n                                data-toggle=\"modal\"\r\n                                (click) = \"confirmShop()\"\r\n                                [disabled] = \"!iTaxConditionSelected || !iPaymentSelected\"\r\n                                data-target=\"#exampleModal\">\r\n                            <span>Confirmar </span> \r\n                            <i class=\"fa fa-print\"></i>\r\n                        </button> -->\r\n                        <button class=\"btn btn-success w-100\"\r\n                                (click) = \"confirmShop()\"\r\n                                [disabled] = \"!iTaxConditionSelected || !iPaymentSelected\">\r\n                            <span>Confirmar </span> \r\n                            <i class=\"fa fa-print\"></i>\r\n                        </button>\r\n                      </div>\r\n                 </div>\r\n             </div>\r\n         </div>\r\n </div>\r\n\r\n <!-- Modal -->\r\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n        <div class=\"modal-header text-center\">\r\n            <h5 class=\"modal-title text-center\" id=\"exampleModalLabel\">Espere por favor</h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <div class=\"sk-fading-circle\">\r\n                <div class=\"sk-circle1 sk-circle\"></div>\r\n                <div class=\"sk-circle2 sk-circle\"></div>\r\n                <div class=\"sk-circle3 sk-circle\"></div>\r\n                <div class=\"sk-circle4 sk-circle\"></div>\r\n                <div class=\"sk-circle5 sk-circle\"></div>\r\n                <div class=\"sk-circle6 sk-circle\"></div>\r\n                <div class=\"sk-circle7 sk-circle\"></div>\r\n                <div class=\"sk-circle8 sk-circle\"></div>\r\n                <div class=\"sk-circle9 sk-circle\"></div>\r\n                <div class=\"sk-circle10 sk-circle\"></div>\r\n                <div class=\"sk-circle11 sk-circle\"></div>\r\n                <div class=\"sk-circle12 sk-circle\"></div>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n        </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pos/components/shopping-cart/shopping-cart.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pos/components/shopping-cart/shopping-cart.component.ts ***!
  \*************************************************************************/
/*! exports provided: ShoppingCartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartComponent", function() { return ShoppingCartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/pos.service */ "./src/app/pos/services/pos.service.ts");
/* harmony import */ var _core_model_Sale__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/model/Sale */ "./src/app/pos/core/model/Sale.ts");
/* harmony import */ var _core_model_User__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/model/User */ "./src/app/pos/core/model/User.ts");
/* harmony import */ var _core_model_ProductSale__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/model/ProductSale */ "./src/app/pos/core/model/ProductSale.ts");
/* harmony import */ var _services_qz_tray_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/qz-tray-service.service */ "./src/app/pos/services/qz-tray-service.service.ts");
/* harmony import */ var _core_model_Alert__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/model/Alert */ "./src/app/pos/core/model/Alert.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ShoppingCartComponent = /** @class */ (function () {
    function ShoppingCartComponent(_PosService, _QZService) {
        this._PosService = _PosService;
        this._QZService = _QZService;
        this.iProducts = []; // Mantiene el listado de los productos a vender
        this.iPaymentMethods = []; // Controla los metodos a pagar
        this.iTaxConditions = []; // Controla los impuestos a incluir
        this.iTotalPrice = 0; // Controla el valor total de la compra
    }
    ShoppingCartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._PosService.getCart().subscribe(function (res) {
            _this.iProducts.push(res);
            _this.calculatePrice();
        });
        this._PosService.getTaxConditions().subscribe(function (res) {
            _this.iTaxConditions = res['result'];
        });
        this._PosService.getPaymentsMethods().subscribe(function (res) {
            _this.iPaymentMethods = res['result'];
        });
    };
    ShoppingCartComponent.prototype.calculatePrice = function () {
        var _this = this;
        this.iDiscount > 100 ? this.iDiscount = 100 :
            this.iDiscount < 0 ? this.iDiscount = 0 :
                this.iTotalPrice = 0;
        this.iProducts.forEach(function (x) {
            return _this.iTotalPrice += Number(x.price);
        });
        // Intenta aplicarle descuento
        if (this.iDiscount) {
            this.iTotalPrice = this.iTotalPrice - (this.iTotalPrice * (this.iDiscount / 100));
        }
    };
    ShoppingCartComponent.prototype.deleteFromCart = function (pProductID) {
        var mIndex = this.iProducts.findIndex(function (x) { return x.id === pProductID; });
        if (mIndex > -1) {
            this._PosService.setProductToReturn(this.iProducts[mIndex]);
            this.iProducts.splice(mIndex, 1);
            this.calculatePrice();
        }
    };
    ShoppingCartComponent.prototype.setPaymentMethod = function (pId) {
        var mIndex = this.iPaymentMethods.findIndex(function (item) { return item.id === pId; });
        this.iPaymentSelected = this.iPaymentMethods[mIndex];
    };
    ShoppingCartComponent.prototype.setTaxCondition = function (pId) {
        var mIndex = this.iTaxConditions.findIndex(function (item) { return item.id === pId; });
        this.iTaxConditionSelected = this.iTaxConditions[mIndex];
    };
    ShoppingCartComponent.prototype.confirmShop = function () {
        var _this = this;
        // this._QZService.printTicket();
        if (!this.iDiscount) {
            this.iDiscount = 0;
        }
        var mProducSales = [];
        this.iProducts.forEach(function (item) {
            var mProductSale = new _core_model_ProductSale__WEBPACK_IMPORTED_MODULE_4__["ProductSale"](1, Number(item.id));
            mProducSales.push(mProductSale);
        });
        console.log(this.iCurrentUser);
        console.log(this.iCurrentUser.id);
        var mSale = new _core_model_Sale__WEBPACK_IMPORTED_MODULE_2__["Sale"](this.iPaymentSelected.id, Number(this.iCurrentUser.id), this.iDiscount, this.iTaxConditionSelected.id, mProducSales);
        console.log(mProducSales);
        this._PosService.addNewSale(mSale).subscribe(function (res) {
            _this._PosService.setAlertToShow(new _core_model_Alert__WEBPACK_IMPORTED_MODULE_6__["Alert"]('Operación exitosa', 1));
        }, function (error) { return _this._PosService.setAlertToShow(new _core_model_Alert__WEBPACK_IMPORTED_MODULE_6__["Alert"]('Hubo un error', 2)); });
        this.iProducts = [];
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _core_model_User__WEBPACK_IMPORTED_MODULE_3__["User"])
    ], ShoppingCartComponent.prototype, "iCurrentUser", void 0);
    ShoppingCartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-shopping-cart',
            template: __webpack_require__(/*! ./shopping-cart.component.html */ "./src/app/pos/components/shopping-cart/shopping-cart.component.html"),
            styles: [__webpack_require__(/*! ./shopping-cart.component.css */ "./src/app/pos/components/shopping-cart/shopping-cart.component.css")]
        }),
        __metadata("design:paramtypes", [_services_pos_service__WEBPACK_IMPORTED_MODULE_1__["PosService"],
            _services_qz_tray_service_service__WEBPACK_IMPORTED_MODULE_5__["QzTrayService"]])
    ], ShoppingCartComponent);
    return ShoppingCartComponent;
}());



/***/ }),

/***/ "./src/app/pos/core/model/Alert.ts":
/*!*****************************************!*\
  !*** ./src/app/pos/core/model/Alert.ts ***!
  \*****************************************/
/*! exports provided: Alert */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Alert", function() { return Alert; });
var Alert = /** @class */ (function () {
    /**
     *
     */
    function Alert(pMessage, pTypeMessage) {
        this.message = pMessage;
        this.typeMessage = pTypeMessage;
    }
    return Alert;
}());



/***/ }),

/***/ "./src/app/pos/core/model/ProductSale.ts":
/*!***********************************************!*\
  !*** ./src/app/pos/core/model/ProductSale.ts ***!
  \***********************************************/
/*! exports provided: ProductSale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductSale", function() { return ProductSale; });
var ProductSale = /** @class */ (function () {
    /**
     *
     */
    function ProductSale(mQuantity, mProductID) {
        this.quantity = mQuantity;
        this.product_id = mProductID;
    }
    return ProductSale;
}());



/***/ }),

/***/ "./src/app/pos/core/model/Sale.ts":
/*!****************************************!*\
  !*** ./src/app/pos/core/model/Sale.ts ***!
  \****************************************/
/*! exports provided: Sale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sale", function() { return Sale; });
var Sale = /** @class */ (function () {
    /**
     *
     */
    function Sale(pPaymentMethodID, pSellerID, pDiscount, pTaxConditionID, pProductSale) {
        this.payment_method_id = pPaymentMethodID;
        this.seller_id = pSellerID;
        this.discount = pDiscount;
        this.tax_condition_id = pTaxConditionID;
        this.products = pProductSale;
    }
    return Sale;
}());



/***/ }),

/***/ "./src/app/pos/core/model/User.ts":
/*!****************************************!*\
  !*** ./src/app/pos/core/model/User.ts ***!
  \****************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    /**
     *
     */
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/pos/pipes/product-search.pipe.ts":
/*!**************************************************!*\
  !*** ./src/app/pos/pipes/product-search.pipe.ts ***!
  \**************************************************/
/*! exports provided: ProductSearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductSearchPipe", function() { return ProductSearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ProductSearchPipe = /** @class */ (function () {
    function ProductSearchPipe() {
    }
    ProductSearchPipe.prototype.transform = function (items, searchText) {
        if (!items) {
            return [];
        }
        if (!searchText) {
            return items;
        }
        searchText = searchText.toLowerCase();
        return items.filter(function (it) {
            return (it.name.toLowerCase().includes(searchText) ||
                it.description.toLowerCase().includes(searchText) ||
                it.code_bar.includes(searchText));
        });
    };
    ProductSearchPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'productSearch'
        })
    ], ProductSearchPipe);
    return ProductSearchPipe;
}());



/***/ }),

/***/ "./src/app/pos/pos/pos.component.css":
/*!*******************************************!*\
  !*** ./src/app/pos/pos/pos.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pos/pos/pos.component.html":
/*!********************************************!*\
  !*** ./src/app/pos/pos/pos.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-12 h-100 pos-component\" *ngIf=\"!iLoadingConfiguration\" appScannerBarcode>\r\n  <div class=\"navbar-top\">\r\n    <app-navbar \r\n      class=\"w-100\"\r\n      [iCurrentUser] = \"iCurrentUser\">\r\n    </app-navbar>\r\n  </div>\r\n  <app-alerts></app-alerts>\r\n  <div class=\"row shop-component\">\r\n      <div class=\"col-4\">\r\n        <app-shopping-cart \r\n          class=\"h-100\"\r\n          [iCurrentUser] = \"iCurrentUser\">\r\n        </app-shopping-cart>\r\n      </div>\r\n      <div class=\"col-8\">\r\n        <app-products class=\"h-100\"></app-products>\r\n      </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"loading\" *ngIf=\"iLoadingConfiguration\">Loading&#8230;\r\n  hola\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pos/pos/pos.component.ts":
/*!******************************************!*\
  !*** ./src/app/pos/pos/pos.component.ts ***!
  \******************************************/
/*! exports provided: PosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PosComponent", function() { return PosComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/pos.service */ "./src/app/pos/services/pos.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PosComponent = /** @class */ (function () {
    function PosComponent(_PosService) {
        this._PosService = _PosService;
        this.iLoadingConfiguration = true;
    }
    PosComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Me subscribo a la carga de configuracion
        this._PosService.setLoadingConfiguration(true);
        this._PosService.getLoadingConfiguration().subscribe(function (res) { return _this.iLoadingConfiguration = res; });
        // Obtengo el usuario loggeado y datos iniciales
        this._PosService.getUserLogged().subscribe(function (resuserLogged) {
            _this.iCurrentUser = resuserLogged['result'];
            // Aviso que ya termino de cargar la data
            _this._PosService.setLoadingConfiguration(false);
        }, function (error) { return console.log(error); });
    };
    PosComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pos',
            template: __webpack_require__(/*! ./pos.component.html */ "./src/app/pos/pos/pos.component.html"),
            styles: [__webpack_require__(/*! ./pos.component.css */ "./src/app/pos/pos/pos.component.css")]
        }),
        __metadata("design:paramtypes", [_services_pos_service__WEBPACK_IMPORTED_MODULE_1__["PosService"]])
    ], PosComponent);
    return PosComponent;
}());



/***/ }),

/***/ "./src/app/pos/scanner-barcode.directive.ts":
/*!**************************************************!*\
  !*** ./src/app/pos/scanner-barcode.directive.ts ***!
  \**************************************************/
/*! exports provided: ScannerBarcodeDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScannerBarcodeDirective", function() { return ScannerBarcodeDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_pos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/pos.service */ "./src/app/pos/services/pos.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ScannerBarcodeDirective = /** @class */ (function () {
    function ScannerBarcodeDirective(_PosService) {
        this._PosService = _PosService;
        this.iKey = 13;
        this.iString = '';
    }
    ScannerBarcodeDirective.prototype.onKeydownHandler = function (event) {
        /* setInterval(() => {
          if (event.keyCode !== this.key) {
            this.iString = '';
          } TODO: analizar cada cuanto se limpiaria
        }, 3000); */
        if (event.keyCode === this.iKey) {
            console.log('buscar', this.iString);
            this._PosService.barCodeProduct(this.iString);
            this.iString = '';
        }
        else {
            this.iString += event.key;
            console.log(this.iString);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('document:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], ScannerBarcodeDirective.prototype, "onKeydownHandler", null);
    ScannerBarcodeDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appScannerBarcode]'
        }),
        __metadata("design:paramtypes", [_services_pos_service__WEBPACK_IMPORTED_MODULE_1__["PosService"]])
    ], ScannerBarcodeDirective);
    return ScannerBarcodeDirective;
}());



/***/ }),

/***/ "./src/app/pos/services/pos.service.ts":
/*!*********************************************!*\
  !*** ./src/app/pos/services/pos.service.ts ***!
  \*********************************************/
/*! exports provided: PosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PosService", function() { return PosService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PosService = /** @class */ (function () {
    // 'http://qa.escalantef.com/pos';
    function PosService(_httpClient) {
        this._httpClient = _httpClient;
        this.iLoading = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"](); // Controla la visualización o no de la vista
        this.iAlerts = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"](); // Controla la visualización o no de la vista
        this.iCart = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"](); // Carrito de compras
        this.iProductToReturn = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"](); // Productos a volver a poner en el listado
        this.iProductToDelete = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"](); // Productos a quitar del listado (por codigo de barra)
        this.iProducts = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"](); // Productos a volver a poner en el listado
        this.iApiLocation = window.location.origin + '/' + window.location.pathname.split('/')[1];
        console.log(this.iApiLocation);
    }
    // Devuelve un observable con la informacion del usuario loggeado
    PosService.prototype.getUserLogged = function () {
        return this._httpClient.get(this.iApiLocation + '/Users/getUserLogged');
    };
    // Devuelve un listado de los productos para vender
    PosService.prototype.getProductList = function () {
        return this._httpClient.get(this.iApiLocation + '/Products/getProducts');
    };
    PosService.prototype.getPaymentsMethods = function () {
        return this._httpClient.get(this.iApiLocation + '/Sales/getPaymentMethods');
    };
    PosService.prototype.getTaxConditions = function () {
        return this._httpClient.get(this.iApiLocation + '/Sales/getTaxConditions');
    };
    // Mantiene y setea el estado del punto de venta (cargando configuracion o no)
    PosService.prototype.getLoadingConfiguration = function () { return this.iLoading.asObservable(); };
    PosService.prototype.setLoadingConfiguration = function (pLoad) { this.iLoading.next(pLoad); };
    // Mantiene y setea el estado del punto de venta (cargando configuracion o no)
    PosService.prototype.getCart = function () { return this.iCart.asObservable(); };
    PosService.prototype.setProductToCart = function (pProduct) { this.iCart.next(pProduct); };
    // Metodo para agregar un producto por scanner
    PosService.prototype.barCodeProduct = function (pBarcode) {
        this.addProductByCodeBar(pBarcode);
    };
    // Mantiene los productos a quitar del carrito y devolver al listado
    PosService.prototype.getProductToReturn = function () { return this.iProductToReturn.asObservable(); };
    PosService.prototype.setProductToReturn = function (pProducts) { this.iProductToReturn.next(pProducts); };
    // Mantiene el listado de productos
    PosService.prototype.getProducts = function () { return this.iProducts.asObservable(); };
    PosService.prototype.initializeProducts = function () {
        var _this = this;
        this.getProductList().subscribe(function (res) {
            _this.iProducts.next(res['result']);
        });
    };
    // Mantiene los productos a quitar del carrito y devolver al listado
    PosService.prototype.getProductToDelete = function () { return this.iProductToDelete.asObservable(); };
    PosService.prototype.addProductByCodeBar = function (pProductId) { this.iProductToDelete.next(pProductId); };
    PosService.prototype.addNewSale = function (pSale) {
        console.log(pSale);
        var mHeader = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Content-Type', 'application/json');
        var mBody = JSON.stringify(pSale);
        console.log(mBody);
        return this._httpClient.post(this.iApiLocation + '/Sales/saveSale', mBody, { headers: mHeader });
    };
    // Mantiene y setea los mensajes de error y exito
    // Mantiene los productos a quitar del carrito y devolver al listado
    PosService.prototype.getAlertToshow = function () { return this.iAlerts.asObservable(); };
    PosService.prototype.setAlertToShow = function (pAlert) { this.iAlerts.next(pAlert); };
    PosService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PosService);
    return PosService;
}());



/***/ }),

/***/ "./src/app/pos/services/qz-tray-service.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/pos/services/qz-tray-service.service.ts ***!
  \*********************************************************/
/*! exports provided: QzTrayService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QzTrayService", function() { return QzTrayService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var QzTrayService = /** @class */ (function () {
    function QzTrayService() {
        this.ticket = {
            nombreComercio: 'CANELA ROPA',
            nombreDuenio: 'SIGOT MARIELA VIVIANA',
            cuit: 'Nro.: 27-250256179-7',
            direccion: 'ROCAMORA 651 * C. DEL URUGUAY * 3260 * E.R.',
            pv: '',
            nroTicket: '',
            fechayhora: '',
            productos: [],
            total: 0,
            saludo: 'GRACIAS POR SU COMPRA',
        };
    }
    QzTrayService.prototype.configQZ = function () {
        qz.security.setCertificatePromise(function (resolve, reject) {
            // Preferred method - from server
            $.ajax('./../../../assets/qz/override.crt\')?>').then(resolve, reject);
        });
        qz.security.setSignaturePromise(function (toSign) {
            return function (resolve, reject) {
                $.post('./../../../assets/qz/assets/signing/sign-message.js', { request: toSign }).then(resolve, reject);
            };
        });
    };
    /**
     * Imprime el ticket
     */
    QzTrayService.prototype.generateTicket = function () {
        var cadena_final = [];
        // init ticket
        cadena_final.push('\n');
        // centrado
        cadena_final.push('\x1D' + '\x21' + '\x00');
        cadena_final.push('\x1B' + '\x61' + '\x31' + this.ticket.nombreComercio);
        cadena_final.push('\n');
        cadena_final.push('-------------------');
        cadena_final.push('\n');
        cadena_final.push(this.ticket.nombreDuenio);
        cadena_final.push('\n');
        cadena_final.push('==============================================');
        cadena_final.push('\n');
        // alinear izq
        cadena_final.push('\x1B' + '\x61' + '\x30');
        cadena_final.push('Ticket Nro. : ' + this.ticket.nroTicket);
        cadena_final.push('\n');
        cadena_final.push('Fecha : ' + this.ticket.fechayhora);
        cadena_final.push('\n');
        // centrado
        cadena_final.push('\x1B' + '\x61' + '\x31');
        cadena_final.push('----------------------------------------------');
        cadena_final.push('\n');
        cadena_final.push('//////////////  //////////////');
        cadena_final.push('\n');
        cadena_final.push('----------------------------------------------');
        cadena_final.push('\n');
        console.log(cadena_final);
        return cadena_final;
    };
    QzTrayService.prototype.printTicket = function () {
        this.configQZ();
        var cadena = this.generateTicket();
        console.log(qz.websocket.isActive());
        if (qz.websocket.isActive()) {
            var config = qz.configs.create(print);
            return qz.print(config, cadena);
        }
        else {
            qz.websocket.connect().then(function () {
                return qz.printers.find('epson');
            })
                .then(function (printer) {
                var config = qz.configs.create(printer);
                return qz.print(config, cadena);
            })
                .catch(function (e) { console.error(e); });
        }
    };
    QzTrayService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], QzTrayService);
    return QzTrayService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\facun\OneDrive\Escritorio\code\canela-pos\Angular\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map